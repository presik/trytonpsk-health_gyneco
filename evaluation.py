from trytond.model import fields, ModelSQL
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval, Id, Equal, If, Bool


class PatientEvaluation(metaclass=PoolMeta):
    __name__ = 'health.patient.evaluation'
    pregnancies = fields.Function(fields.One2Many('health.patient.pregnancy',
        'evaluation', 'Pregnancies', states={
            'invisible': If(
                Eval('specialty', None),
                ~Eval('specialty', 0).in_([
                    Id('health', 'obstetrics'),
                    Id('health', 'gynecology'),
                    Id('health', 'obstetrics_gynecology'),
                ]),
                True
            ),
            'readonly': Eval('state') == 'signed'
        },
        depends=['specialty'],
        domain=[('patient', '=', Eval('patient'))]
    ), 'get_pregnancies', setter='set_pregnancies')
    evaluations_pregnancies = fields.Many2Many(
        'health.evaluation-health.patient.pregnancy',
        'evaluation', 'pregnancy', 'Pregnancies')

    def get_pregnancies(self, name=None):
        Pregnancy = Pool().get('health.patient.pregnancy')
        if self.patient.id:
            pregnancies = Pregnancy.search_read([
                ('patient', '=', self.patient.id)
            ])
            return [p['id'] for p in pregnancies]

    @classmethod
    def set_pregnancies(cls, records, name, value):
        Pregnancy = Pool().get('health.patient.pregnancy')
        PregnancyRel = Pool().get('health.evaluation-health.patient.pregnancy')
        if not value:
            return
        for val in value:
            if val[0] == 'create':
                Pregnancy.create(val[1])
            elif val[0] == 'write':
                ids, data = val[1], val[2]
                pregnacies = Pregnancy.browse(ids)
                Pregnancy.write(pregnacies, data)


class HealthEvaluationPatientPregnancy(ModelSQL):
    'Health Evaluation - Patient Pregnancy'
    __name__ = 'health.evaluation-health.patient.pregnancy'
    _table = 'health_evaluation_health_patient_pregnancy_rel'
    evaluation = fields.Many2One('health.patient.evaluation', 'Evaluation',
        ondelete='CASCADE', select=True, required=True)
    pregnancy = fields.Many2One('health.patient.pregnancy', 'Pregnancy',
        ondelete='CASCADE', select=True, required=True)
