# -*- coding: utf-8 -*-
from datetime import timedelta, date, datetime
from sql import Table

from trytond.i18n import gettext
from trytond.exceptions import UserError
from trytond.model import ModelView, ModelSQL, fields, Unique
from trytond.pyson import Eval, Not, Bool, Equal
from trytond.pool import Pool
from trytond.transaction import Transaction
from sql.aggregate import Count

YES_NO = [
    ('yes', 'Yes'),
    ('no', 'No'),
]

POS_NEG = [
    ('positive', 'Positive'),
    ('negative', 'Negative'),
]

REACTIVE = [
    ('reactive', 'Reactive'),
    ('no_reactive', 'No Reactive'),
]

class Patient(ModelSQL, ModelView):
    """
    Add to the Medical patient data gynecological and obstetric fields.
    """
    __name__ = 'health.patient'
    currently_pregnant = fields.Function(fields.Boolean('Pregnant'),
        'get_pregnancy_info')
    fertile = fields.Boolean('Fertile',
        help="Check if patient is in fertile age")
    menarche = fields.Integer('Menarche age')
    menopausal = fields.Boolean('Menopausal')
    menopause = fields.Integer('Menopause age')
    mammography = fields.Boolean('Mammography',
        help="Check if the patient does periodic mammographys")
    mammography_last = fields.Date('Last mammography',
        help="Enter the date of the last mammography")
    breast_self_examination = fields.Boolean('Breast self-examination',
        help="Check if patient does and knows how to self examine her breasts")
    pap_test = fields.Boolean('PAP test',
        help="Check if patient does periodic cytologic pelvic smear screening")
    pap_test_last = fields.Date('Last PAP test',
        help="Enter the date of the last Papanicolau test")
    colposcopy = fields.Boolean('Colposcopy',
        help="Check if the patient has done a colposcopy exam")
    colposcopy_last = fields.Date('Last colposcopy',
        help="Enter the date of the last colposcopy")
    pregnancies = fields.Integer('Pregnancies',
        help="Number of pregnancies, computed from Obstetric history")
    births = fields.Integer('Births')
    premature = fields.Integer('Premature', help="Preterm < 37 wks live births")
    abortions = fields.Integer('Abortions')
    stillbirths = fields.Integer('Stillbirths')
    by_cesareas = fields.Integer('By Cesareas')

    full_term = fields.Integer('Full Term', help="Full term pregnancies")
    menstrual_history = fields.One2Many('health.patient.menstrual_history',
        'patient', 'Menstrual History')
    mammography_history = fields.One2Many(
        'health.patient.mammography_history', 'patient', 'Mammography History',
         states={'invisible': Not(Bool(Eval('mammography')))},
        )
    pap_history = fields.One2Many('health.patient.pap_history', 'patient',
        'PAP smear History',
         states={'invisible': Not(Bool(Eval('pap_test')))},
        )
    colposcopy_history = fields.One2Many(
        'health.patient.colposcopy_history', 'patient', 'Colposcopy History',
         states={'invisible': Not(Bool(Eval('colposcopy')))},
        )
    pregnancy_history = fields.One2Many('health.patient.pregnancy', 'patient',
        'Pregnancies')

    def get_pregnancy_info(self, name):
        if name == 'currently_pregnant':
            for pregnancy_history in self.pregnancy_history:
                if pregnancy_history.current_pregnancy:
                    return True
        return False

    def patient_obstetric_info(self, name):
        ''' Return the number of pregnancies, perterm,
        abortion and stillbirths '''

        counter = 0
        pregnancies = len(self.pregnancy_history)

        if (name == "gravida"):
            return pregnancies

        if (name == "premature"):
            prematures = 0
            while counter < pregnancies:
                result = self.pregnancy_history[counter].pregnancy_end_result
                preg_weeks = self.pregnancy_history[counter].pregnancy_end_age
                if (result == "live_birth" and preg_weeks < 37):
                    prematures = prematures + 1
                counter = counter + 1
            return prematures

        if (name == "abortions"):
            abortions = 0
            while counter < pregnancies:
                result = self.pregnancy_history[counter].pregnancy_end_result
                preg_weeks = self.pregnancy_history[counter].pregnancy_end_age
                if (result == "abortion"):
                    abortions = abortions + 1
                counter = counter + 1

            return abortions

        if (name == "stillbirths"):
            stillbirths = 0
            while counter < pregnancies:
                result = self.pregnancy_history[counter].pregnancy_end_result
                preg_weeks = self.pregnancy_history[counter].pregnancy_end_age
                if (result == "stillbirth"):
                    stillbirths = stillbirths + 1
                counter = counter+1
            return stillbirths

    @classmethod
    def view_attributes(cls):
        return [('//page[@id="page_gyneco_obs"]', 'states', {
            'invisible': Equal(Eval('biological_sex'), 'm'),
        })]


class PatientPregnancy(ModelSQL, ModelView):
    'Patient Pregnancy - Obstetric History'
    __name__ = 'health.patient.pregnancy'
    CERVIX_STATE = {
        'invisible': Eval('cervix', '') == 'yes'
    }
    patient = fields.Many2One('health.patient', 'Patient ID')
    gravida = fields.Integer('Pregnancy #', required=True)
    high_risk = fields.Boolean('High Risk', help='Check this field if this '
        ' pregancy is hight risk')
    warning_icon = fields.Function(fields.Char('Pregnancy warning icon'), 'get_warn_icon')
    blood_group = fields.Function(fields.Char('Blood Group'), 'get_blood_group')
    lmp = fields.Date('LMP', help="Last Menstrual Period", required=True)
    current_weeks = fields.Float('Current Weeks', digits=(4,1),
        depends=['lmp'])
    doubt_lmp = fields.Selection(YES_NO, 'Doubt LMP', required=True,
        help="If patient has doubt about this LMP date")
    pdd = fields.Date('Pregnancy Due Date', states={'readonly': True})
    sonography_date = fields.Date('Sonography Date')
    sonography_weeks = fields.Integer('Weeks in Sonography')
    prenatal_evaluations = fields.One2Many('health.patient.prenatal.evaluation',
        'patient', 'Prenatal Evaluations')
    perinatal = fields.One2Many('health.perinatal', 'patient', 'Perinatal Info')
    puerperium_monitor = fields.One2Many('health.puerperium.monitor',
        'patient', 'Puerperium monitor')
    current_pregnancy = fields.Boolean('Current Pregnancy', help='This field'
        ' marks the current pregnancy')
    fetuses = fields.Integer('Fetuses', required=True)
    multiple = fields.Selection(YES_NO, 'Multiple', required=True)
    tetanus_vaccine = fields.Selection(YES_NO, 'Tetanus Vaccine')
    pregnancy_end_result = fields.Selection([
        (None, ''),
        ('live_birth', 'Live birth'),
        ('abortion', 'Abortion'),
        ('stillbirth', 'Stillbirth'),
        ('status_unknown', 'Status unknown'),
        ], 'Result', sort=False,
            states={
            'invisible': Bool(Eval('current_pregnancy')),
            # 'required': Not(Bool(Eval('current_pregnancy'))),
        })
    pregnancy_end_date = fields.DateTime('End of Pregnancy',
        states={
            'invisible': Bool(Eval('current_pregnancy')),
            # 'required': Not(Bool(Eval('current_pregnancy'))),
        })
    bba = fields.Boolean('BBA', help="Born Before Arrival",
        states={
            'invisible': Bool(Eval('current_pregnancy')),
        })
    home_birth = fields.Boolean('Home Birth', help="Home Birth",
        states={
            'invisible': Bool(Eval('current_pregnancy')),
        })
    pregnancy_end_age = fields.Function(fields.Char('Weeks',
        help='Weeks at the end of pregnancy'), 'get_pregnancy_end_age')
    institution = fields.Many2One('health.institution', 'Institution',
        help="Health center where this initial obstetric record was created")
    healthprof = fields.Many2One(
        'health.professional', 'Health Prof', readonly=True,
        help="Health Professional who created this initial obstetric record")
    evaluation = fields.Many2One('health.patient.evaluation',
        'Patient Evaluation', readonly=True)
    coffee = fields.Selection(YES_NO, 'Coffee')
    coffee_frequency = fields.Char('Frequency', states={
        'invisible': Bool(Eval('coffee', '') != 'yes')
    })
    smoke = fields.Selection(YES_NO, 'Cigarette')
    smoke_frequency = fields.Char('Frequency', states={
        'invisible': Bool(Eval('smoke', '') != 'yes')
    })
    alcohol = fields.Selection(YES_NO, 'Alcohol')
    alcohol_frequency = fields.Char('Frequency', states={
        'invisible': Bool(Eval('alcohol', '') != 'yes')
    })
    other_habits = fields.Char('Others Habits', help="For example drugs consume")
    breast_self_examination = fields.Selection(YES_NO, 'Breast Self Exam',
        help="Check YES, if patient does and knows how to self examine her breasts")
    mammography_last = fields.Date('Last mammography',
        help="Enter the date of the last mammography")
    vdrl_date = fields.Date('VDRL Date', states=CERVIX_STATE,
        help="The date on which the test VDRL was performed")
    vdrl_result = fields.Selection(REACTIVE, 'VDRL Result',
        help="Result of Venereal Disease Research Laboratory",
            states=CERVIX_STATE)
    pap_test = fields.Selection(YES_NO, 'PAP test', states=CERVIX_STATE,
        help="Check YES, if patient does periodic cytologic pelvic smear screening")
    pap_test_last = fields.Date('Last PAP test', states=CERVIX_STATE,
        help="Enter the date of the last Papanicolau test")
    colposcopy = fields.Selection(YES_NO, 'Colposcopy',
        states=CERVIX_STATE,
        help="Check YES, if the result of exam is normal")
    hiv = fields.Selection(POS_NEG, 'HIV',
        help="Check if the exam results was positive")
    hepatitis_b = fields.Selection(POS_NEG, 'Hepatitis B', states=CERVIX_STATE)
    hepatitis_b_date = fields.Date('Date Hepatitis B', states=CERVIX_STATE)
    hepatitis_c = fields.Selection(POS_NEG, 'Hepatitis C', states=CERVIX_STATE)
    hepatitis_c_date = fields.Date('Date Hepatitis C', states=CERVIX_STATE)

    @classmethod
    def __setup__(cls):
        super(PatientPregnancy, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints += [
            ('gravida_uniq', Unique(t, t.patient, t.gravida),
                'This pregnancy code for this patient already exists'),
        ]

    @classmethod
    def validate(cls, pregnancies):
        super(PatientPregnancy, cls).validate(pregnancies)
        for pregnancy in pregnancies:
            pregnancy.check_patient_current_pregnancy()

    def check_patient_current_pregnancy(self):
        ''' Check for only one current pregnancy in the patient '''
        pregnancy = Table('health_patient_pregnancy')
        cursor = Transaction().connection.cursor()
        patient_id = int(self.patient.id)

        cursor.execute(*pregnancy.select(Count(pregnancy.patient),
            where=(pregnancy.current_pregnancy == 'true') &
            (pregnancy.patient == patient_id)))

        records = cursor.fetchone()[0]
        if records > 1:
            raise UserError(gettext(
                'health_gyneco.msg_patient_already_pregnant'
            ))

    @staticmethod
    def default_current_pregnancy():
        return False

    @staticmethod
    def default_fetuses():
        return 1

    @staticmethod
    def default_multiple():
        return 'no'

    @staticmethod
    def default_alcohol():
        return 'no'

    @staticmethod
    def default_smoke():
        return 'no'

    @staticmethod
    def default_coffee():
        return 'no'

    @staticmethod
    def default_tetanus_vaccine():
        return 'no'

    @staticmethod
    def default_hepatitis_b():
        return 'negative'

    @staticmethod
    def default_hepatitis_c():
        return 'negative'

    @staticmethod
    def default_colposcopy():
        return 'no'

    @staticmethod
    def default_cervix():
        return 'no'

    @staticmethod
    def default_hiv():
        return 'negative'

    @staticmethod
    def default_institution():
        HealthInst = Pool().get('health.institution')
        return HealthInst.get_institution()

    @staticmethod
    def default_healthprof():
        HealthProf = Pool().get('health.professional')
        return HealthProf.get_health_professional()

    def get_blood_group(self, name=None):
        if self.patient and self.patient.blood_type and self.patient.rh:
            return self.patient.blood_type + self.patient.rh

    @fields.depends('lmp', 'sonography_date', 'sonography_weeks', 'doubt_lmp')
    def on_change_with_current_weeks(self):
        weeks = 0
        if self.sonography_date and self.sonography_weeks and self.doubt_lmp == 'yes':
            pregnancy_start = self.sonography_date - timedelta(self.sonography_weeks * 7)
            weeks = (date.today() - pregnancy_start).days / 7
        elif self.lmp:
            weeks = (date.today() - self.lmp).days / 7
        if weeks:
            return round(weeks, 1)

    @fields.depends('lmp', 'sonography_date', 'sonography_weeks', 'doubt_lmp')
    def on_change_with_pdd(self):
        """ Calculate the Pregnancy Due Date and the Number of
        weeks at the end of pregnancy when using the Last Menstrual
        Period parameter.
        """
        pdd = None
        if self.sonography_date and self.sonography_weeks and self.doubt_lmp == 'yes':
            pdd = self.sonography_date + timedelta(280 - self.sonography_weeks * 7)
        elif self.lmp:
            pdd = self.lmp + timedelta(days=280)
        return pdd

    def get_pregnancy_end_age(self, name=None):
        if self.pregnancy_end_date:
            gestational_age = date(
                self.pregnancy_end_date) - self.lmp
            return (gestational_age.days) / 7
        else:
            return 0

    def get_warn_icon(self, name):
        if self.high_risk:
            return 'health-warning'


class PrenatalEvaluation(ModelSQL, ModelView):
    'Prenatal and Antenatal Evaluations'
    __name__ = 'health.patient.prenatal.evaluation'
    HIGH_RISK_STATE = {
        'invisible': ~Eval('placenta_evaluation')
    }
    SONOGRAPHY_STATE = {
        'invisible': ~Eval('sonography_findings')
    }
    patient = fields.Many2One('health.patient.pregnancy', 'Patient Pregnancy')
    evaluation = fields.Many2One('health.patient.evaluation',
        'Patient Evaluation', readonly=True)
    evaluation_date = fields.DateTime('Date', required=True)
    gestational_weeks = fields.Function(fields.Integer('Gestational Weeks',
        depends=['evaluation_date']), 'on_change_with_gestational_weeks')
    # gestational_days = fields.Function(fields.Integer('Gestational days',
    #     depends=['evaluation_date']), 'on_change_with_gestational')
    hypertension = fields.Boolean('Hypertension', help='Check this box if the'
        ' mother has hypertension')
    preeclampsia = fields.Boolean('Preeclampsia', help='Check this box if the'
        ' mother has pre-eclampsia')
    overweight = fields.Boolean('Overweight', help='Check this box if the'
        ' mother is overweight or obesity')
    diabetes = fields.Boolean('Diabetes', help='Check this box if the mother'
        ' has glucose intolerance or diabetes')
    invasive_placentation = fields.Selection([
        (None, ''),
        ('normal', 'Normal decidua'),
        ('accreta', 'Accreta'),
        ('increta', 'Increta'),
        ('percreta', 'Percreta'),
        ], 'Placentation', sort=False, states=HIGH_RISK_STATE)
    placenta_previa = fields.Boolean('Placenta Previa', states=HIGH_RISK_STATE)
    vasa_previa = fields.Boolean('Vasa Previa', states=HIGH_RISK_STATE)
    fundal_height = fields.Integer('Fundal Height',
        help="Distance between the symphysis pubis and the uterine fundus "
        "(S-FD) in cm")
    fetus_heart_rate = fields.Integer('Fetus heart rate', help='Fetus heart'
        ' rate')
    efw = fields.Integer('EFW (g)', states=SONOGRAPHY_STATE,
        help="Estimated Fetal Weight (g)")
    fetal_bpd = fields.Integer('BPD (cm)', states=SONOGRAPHY_STATE,
        help="Fetal Biparietal Diameter (cm)")
    fetal_ac = fields.Integer('FAC (cm)', states=SONOGRAPHY_STATE,
        help="Fetal Abdominal Circumference (cm)")
    fetal_hc = fields.Integer('FHC (cm)', states=SONOGRAPHY_STATE,
        help="Fetal Head Circumference (cm)")
    fetal_fl = fields.Integer('FFL (cm)', states=SONOGRAPHY_STATE,
        help="Fetal Femur Length (cm)")
    oligohydramnios = fields.Boolean('Oligohydramnios')
    polihydramnios = fields.Boolean('Polihydramnios')
    iugr = fields.Selection([
        (None, ''),
        ('symmetric', 'Symmetric'),
        ('assymetric', 'Asymmetric'),
        ], 'IUGR', sort=False)
    institution = fields.Many2One('health.institution', 'Institution')
    healthprof = fields.Many2One('health.professional', 'Health Prof',
        readonly=True,
        help="Health Professional in charge, or that who entered the \
            information in the system")
    weight = fields.Float('Weight', digits=(3, 2), help='Weight in kilos')
    systolic = fields.Integer('Systolic Pressure',
        help='Systolic pressure expressed in mmHg')
    diastolic = fields.Integer('Diastolic Pressure',
        help='Diastolic pressure expressed in mmHg')
    placenta_evaluation = fields.Boolean('Placenta Evaluation')
    sonography_findings = fields.Boolean('Sonography Findings')

    @staticmethod
    def default_institution():
        return Pool().get('health.institution').get_institution()

    @staticmethod
    def default_evaluation_date():
        return datetime.now()

    @staticmethod
    def default_healthprof():
        HealthProf = Pool().get('health.professional')
        return HealthProf.get_health_professional()

    @fields.depends('patient')
    def on_change_with_placenta_evaluation(self, name=None):
        if self.patient:
            return self.patient.high_risk
        return False

    @fields.depends('evaluation_date', 'patient')
    def on_change_with_gestational_weeks(self, name=None):
        if self.evaluation_date and self.patient:
            gestational_age = self.evaluation_date.date() - self.patient.lmp
            return int((gestational_age.days) / 7)


class PuerperiumMonitor(ModelSQL, ModelView):
    'Puerperium Monitor'
    __name__ = 'health.puerperium.monitor'

    patient = fields.Many2One('health.patient.pregnancy', 'Patient Pregnancy')
    date = fields.DateTime('Date and Time', required=True)
    # Deprecated in 1.6.4 All the clinical information will be taken at the
    # main evaluation.
    # systolic / diastolic / frequency / temperature
    systolic = fields.Integer('Systolic Pressure')
    diastolic = fields.Integer('Diastolic Pressure')
    frequency = fields.Integer('Heart Frequency')
    temperature = fields.Float('Temperature')
    lochia_amount = fields.Selection([
        (None, ''),
        ('n', 'normal'),
        ('e', 'abundant'),
        ('h', 'hemorrhage'),
        ], 'Lochia amount', sort=False)
    lochia_color = fields.Selection([
        (None, ''),
        ('r', 'rubra'),
        ('s', 'serosa'),
        ('a', 'alba'),
        ], 'Lochia color', sort=False)
    lochia_odor = fields.Selection([
        (None, ''),
        ('n', 'normal'),
        ('o', 'offensive'),
        ], 'Lochia odor', sort=False)
    uterus_involution = fields.Integer('Fundal Height',
        help="Distance between the symphysis pubis and the uterine fundus "
        "(S-FD) in cm")
    institution = fields.Many2One('health.institution', 'Institution')
    healthprof = fields.Many2One(
        'health.professional', 'Health Prof', readonly=True,
        help="Health Professional in charge, or that who entered the \
            information in the system")

    @staticmethod
    def default_institution():
        return Pool().get('health.institution').get_institution()

    @staticmethod
    def default_healthprof():
        return Pool().get('health.professional').get_health_professional()


class Perinatal(ModelSQL, ModelView):
    'Perinatal Information'
    __name__ = 'health.perinatal'

    patient = fields.Many2One('health.patient.pregnancy', 'Patient Pregnancy')
    admission_code = fields.Char('Code')
    gravida_number = fields.Integer('Gravida #')
    abortion = fields.Boolean('Abortion')
    stillbirth = fields.Boolean('Stillbirth')
    admission_date = fields.DateTime('Admission',
        help="Date when she was admitted to give birth", required=True)
    # Prenatal evaluations deprecated in 1.6.4. Will be computed automatically
    prenatal_evaluations = fields.Integer('Prenatal evaluations',
        help="Number of visits to the doctor during pregnancy")
    start_labor_mode = fields.Selection([
        (None, ''),
        ('v', 'Vaginal - Spontaneous'),
        ('ve', 'Vaginal - Vacuum Extraction'),
        ('vf', 'Vaginal - Forceps Extraction'),
        ('c', 'C-section'),
        ], 'Delivery mode', sort=False)
    gestational_weeks = fields.Function(fields.Integer('Gestational wks'),
        'get_perinatal_information')
    gestational_days = fields.Integer('Days')
    fetus_presentation = fields.Selection([
        (None, ''),
        ('cephalic', 'Cephalic'),
        ('breech', 'Breech'),
        ('shoulder', 'Shoulder'),
        ], 'Fetus Presentation', sort=False)
    dystocia = fields.Boolean('Dystocia')
    placenta_incomplete = fields.Boolean('Incomplete',
        help='Incomplete Placenta')
    placenta_retained = fields.Boolean('Retained', help='Retained Placenta')
    abruptio_placentae = fields.Boolean('Abruptio Placentae',
        help='Abruptio Placentae')
    episiotomy = fields.Boolean('Episiotomy')
    #Vaginal tearing and forceps variables are deprecated in 1.6.4.
    #They are included in laceration and delivery mode respectively
    vaginal_tearing = fields.Boolean('Vaginal tearing')
    forceps = fields.Boolean('Forceps')
    monitoring = fields.One2Many('health.perinatal.monitor', 'patient',
        'Monitors')
    laceration = fields.Selection([
        (None, ''),
        ('perineal', 'Perineal'),
        ('vaginal', 'Vaginal'),
        ('cervical', 'Cervical'),
        ('broad_ligament', 'Broad Ligament'),
        ('vulvar', 'Vulvar'),
        ('rectal', 'Rectal'),
        ('bladder', 'Bladder'),
        ('urethral', 'Urethral'),
        ], 'Lacerations', sort=False)
    hematoma = fields.Selection([
        (None, ''),
        ('vaginal', 'Vaginal'),
        ('vulvar', 'Vulvar'),
        ('retroperitoneal', 'Retroperitoneal'),
        ], 'Hematoma', sort=False)
    notes = fields.Text('Notes')
    institution = fields.Many2One('health.institution', 'Institution')
    healthprof = fields.Many2One(
        'health.professional', 'Health Prof', readonly=True,
        help="Health Professional in charge, or that who entered the \
            information in the system")

    @staticmethod
    def default_institution():
        return Pool().get('health.institution').get_institution()

    @staticmethod
    def default_healthprof():
        HealthProf = Pool().get('health.professional')
        return HealthProf.get_health_professional()

    def get_perinatal_information(self, name):
        if name == 'gestational_weeks':
            gestational_age = self.admission_date.date() - self.patient.lmp
            return (gestational_age.days) / 7


class PerinatalMonitor(ModelSQL, ModelView):
    'Perinatal Monitor'
    __name__ = 'health.perinatal.monitor'

    patient = fields.Many2One('health.perinatal',
        'Patient Perinatal Evaluation')
    date = fields.DateTime('Date and Time')
    systolic = fields.Integer('Systolic Pressure')
    diastolic = fields.Integer('Diastolic Pressure')
    contractions = fields.Integer('Contractions')
    frequency = fields.Integer('Mother\'s Heart Frequency')
    dilation = fields.Integer('Cervix dilation')
    fetus_frequency = fields.Integer('Fetus Heart Frequency')
    meconium = fields.Boolean('Meconium')
    bleeding = fields.Boolean('Bleeding')
    fundal_height = fields.Integer('Fundal Height')
    fetus_position = fields.Selection([
        (None, ''),
        ('o', 'Occiput / Cephalic Posterior'),
        ('fb', 'Frank Breech'),
        ('cb', 'Complete Breech'),
        ('t', 'Transverse Lie'),
        ('t', 'Footling Breech'),
        ], 'Fetus Position', sort=False)


class PatientMenstrualHistory(ModelSQL, ModelView):
    'Menstrual History'
    __name__ = 'health.patient.menstrual_history'

    patient = fields.Many2One('health.patient', 'Patient', readonly=True,
        required=True)
    evaluation = fields.Many2One('health.patient.evaluation', 'Evaluation',
        domain=[('patient', '=', Eval('name'))],
        depends=['name'])
    evaluation_date = fields.Date('Date', help="Evaluation Date",
        required=True)
    lmp = fields.Date('LMP', help="Last Menstrual Period", required=True)
    lmp_length = fields.Integer('Length', required=True)
    is_regular = fields.Boolean('Regular')
    dysmenorrhea = fields.Boolean('Dysmenorrhea')
    frequency = fields.Selection([
        ('amenorrhea', 'amenorrhea'),
        ('oligomenorrhea', 'oligomenorrhea'),
        ('eumenorrhea', 'eumenorrhea'),
        ('polymenorrhea', 'polymenorrhea'),
        ], 'frequency', sort=False)
    volume = fields.Selection([
        ('hypomenorrhea', 'hypomenorrhea'),
        ('normal', 'normal'),
        ('menorrhagia', 'menorrhagia'),
        ], 'volume', sort=False)
    institution = fields.Many2One('health.institution', 'Institution')
    healthprof = fields.Many2One('health.professional', 'Reviewed',
        readonly=True, help="Health Professional who reviewed the information")

    @staticmethod
    def default_institution():
        HealthInst = Pool().get('health.institution')
        institution = HealthInst.get_institution()
        return institution

    @staticmethod
    def default_healthprof():
        HealthProf = Pool().get('health.professional')
        return HealthProf.get_health_professional()

    @staticmethod
    def default_evaluation_date():
        return Pool().get('ir.date').today()

    @staticmethod
    def default_frequency():
        return 'eumenorrhea'

    @staticmethod
    def default_volume():
        return 'normal'


class PatientMammographyHistory(ModelSQL, ModelView):
    'Mammography History'
    __name__ = 'health.patient.mammography_history'

    patient = fields.Many2One('health.patient', 'Patient', readonly=True,
        required=True)
    evaluation = fields.Many2One('health.patient.evaluation', 'Evaluation',
        domain=[('patient', '=', Eval('patient'))],
        depends=['patient'])
    evaluation_date = fields.Date('Date', help="Date", required=True)
    last_mammography = fields.Date('Previous', help="Last Mammography")
    result = fields.Selection([
        (None, ''),
        ('normal', 'normal'),
        ('abnormal', 'abnormal'),
        ], 'result', help="Please check the lab test results if the module is \
            installed", sort=False)
    comments = fields.Char('Remarks')
    institution = fields.Many2One('health.institution', 'Institution')
    healthprof = fields.Many2One(
        'health.professional', 'Reviewed', readonly=True,
        help="Health Professional who last reviewed the test")

    @staticmethod
    def default_institution():
        return Pool().get('health.institution').get_institution()

    @staticmethod
    def default_healthprof():
        HealthProf = Pool().get('health.professional')
        return HealthProf.get_health_professional()

    @staticmethod
    def default_evaluation_date():
        return Pool().get('ir.date').today()

    @staticmethod
    def default_last_mammography():
        return Pool().get('ir.date').today()


class PatientPAPHistory(ModelSQL, ModelView):
    'PAP Test History'
    __name__ = 'health.patient.pap_history'

    patient = fields.Many2One('health.patient', 'Patient', readonly=True,
        required=True)
    evaluation = fields.Many2One('health.patient.evaluation', 'Evaluation',
        domain=[('patient', '=', Eval('patient'))],
        depends=['patient'])
    evaluation_date = fields.Date('Date', help="Date", required=True)
    last_pap = fields.Date('Previous', help="Last Papanicolau")
    result = fields.Selection([
        (None, ''),
        ('negative', 'Negative'),
        ('c1', 'ASC-US'),
        ('c2', 'ASC-H'),
        ('g1', 'ASG'),
        ('c3', 'LSIL'),
        ('c4', 'HSIL'),
        ('g4', 'AIS'),
        ], 'result', help="Please check the lab results if the module is \
            installed", sort=False)
    comments = fields.Char('Remarks')
    institution = fields.Many2One('health.institution', 'Institution')
    healthprof = fields.Many2One('health.professional', 'Reviewed',
        readonly=True,
        help="Health Professional who last reviewed the test")

    @staticmethod
    def default_institution():
        HealthInst = Pool().get('health.institution')
        institution = HealthInst.get_institution()
        return institution

    @staticmethod
    def default_healthprof():
        HealthProf = Pool().get('health.professional')
        return HealthProf.get_health_professional()

    @staticmethod
    def default_evaluation_date():
        return Pool().get('ir.date').today()

    @staticmethod
    def default_last_pap():
        return Pool().get('ir.date').today()


class PatientColposcopyHistory(ModelSQL, ModelView):
    'Colposcopy History'
    __name__ = 'health.patient.colposcopy_history'

    patient = fields.Many2One('health.patient', 'Patient', readonly=True,
        required=True)
    evaluation = fields.Many2One('health.patient.evaluation', 'Evaluation',
        domain=[('patient', '=', Eval('patient'))],
        depends=['patient'])
    evaluation_date = fields.Date('Date', help="Date", required=True)
    last_colposcopy = fields.Date('Previous', help="Last colposcopy")
    result = fields.Selection([
        (None, ''),
        ('normal', 'normal'),
        ('abnormal', 'abnormal'),
        ], 'result', help="Please check the lab test results if the module is \
            installed", sort=False)
    comments = fields.Char('Remarks')
    institution = fields.Many2One('health.institution', 'Institution')
    healthprof = fields.Many2One(
        'health.professional', 'Reviewed', readonly=True,
        help="Health Professional who last reviewed the test")

    @staticmethod
    def default_institution():
        HealthInst = Pool().get('health.institution')
        return HealthInst.get_institution()

    @staticmethod
    def default_healthprof():
        HealthProf = Pool().get('health.professional')
        return HealthProf.get_health_professional()

    @staticmethod
    def default_evaluation_date():
        return Pool().get('ir.date').today()

    @staticmethod
    def default_last_colposcopy():
        return Pool().get('ir.date').today()
